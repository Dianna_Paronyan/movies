import fLogo from '/assets/FeaturedTitleImage.png';
import {useEffect, useRef} from "react";
import './FeaturedVideo.css'
export default function FeaturedVideo({videoData, clickedMovie}){
    const videoRef = useRef(null);
    useEffect(()=>{
        if(clickedMovie) {
            document.body.style.background = `url(${clickedMovie.VideoUrl}) no-repeat center center`;
            setTimeout(() => {
                videoRef.current.src = clickedMovie.VideoUrl;
                videoRef.current.play();
            }, 2000);
        }
    },[clickedMovie])

    function duration(durationInSeconds){
        const hours = Math.floor(durationInSeconds / 3600);
        const minutes = Math.floor((durationInSeconds % 3600) / 60);
        return `${hours}h ${minutes}m`
    }
    return (
        <div className='info'>
            {
                !clickedMovie ? (
                    <div className="video-details">
                        <div>
                            <div className="category">{videoData?.Category}</div>
                            <img src={fLogo} alt="Movie Logo" className="movie-logo" />
                        </div>
                        <div className='movie-info'>
                            <div className="release-year">{videoData?.ReleaseYear}</div>
                            <div className="rating">{videoData?.MpaRating}</div>
                            <div className="duration">{duration(videoData?.Duration)}</div>

                        </div>
                        <div className="description">{videoData?.Description}</div>
                        <div className="buttons">
                            <button className="play-button">{'\u25B6'} Play</button>
                            <button className="info-button">More Info</button>
                        </div>
                    </div>
                ) : (
                    <div className="video-details">
                        <div>
                            <div className="category">{videoData?.Category}</div>
                            <img src={fLogo} alt="Movie Logo" className="movie-logo" />
                        </div>
                        <div className="title">{videoData?.Title}</div>
                        <div className='movie-info'>
                            <div className="release-year">{videoData?.ReleaseYear}</div>
                            <div className="rating">{videoData?.MpaRating}</div>
                            <div className="duration">{duration(videoData?.Duration)}</div>
                        </div>
                        <div className="description">{videoData?.Description}</div>
                        <div className="video-background-overlay"></div>
                        <video ref={videoRef} controls={false} />
                    </div>
                )
            }
        </div>
    )
}