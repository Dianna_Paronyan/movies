import search from '/assets/icons/ICON - Search.png';
import home from '/assets/icons/home.png';
import shows from '/assets/icons/tv-shows.png';
import movies from '/assets/icons/movies.png';
import genres from '/assets/icons/genres.png';
import later from '/assets/icons/later.png';
import './MainMenu.css';

const icons = [
    { src: search, alt: 'Search', text: 'Search' },
    { src: home, alt: 'Home', text: 'Home' },
    { src: shows, alt: 'TV Shows', text: 'TV Shows' },
    { src: movies, alt: 'Movies', text: 'Movies' },
    { src: genres, alt: 'Genres', text: 'Genres' },
    { src: later, alt: 'Watch Later', text: 'Watch Later' },
];
export default function MainMenu(){
    return(
        <div style={{position:'relative'}}>
        <div className='menu-container'>
            <div className="menu-content">
                <div className="profile">
                    <img src='https://www.pngmart.com/files/21/Account-User-PNG.png' alt='profile' className='profile-icon' />
                    <div className="profile-name">Daniel</div>
                </div>
                <div className='icons'>
                    {icons.map((icon, idx) => (
                        <div key={idx} className="icon-container">
                            <img src={icon.src} alt={icon.alt} className='menu-icon' />
                            <div className="icon-text">{icon.text}</div>
                        </div>
                    ))}
                </div>
                <div className="additional-info">
                    <div className="menu-item">Language</div>
                    <div className="menu-item">Get Help</div>
                    <div className="menu-item">Exit</div>
                </div>
            </div>
        </div>
        </div>
    )
}