
import {useRef} from "react";
import './Trending.css'
export default function Trending({trending,  onMovieClick}){
    const carouselRef = useRef(null);

    return(
        <div className='container'>
            <div className="carousel-container">
                <div className="carousel" ref={carouselRef}>
                    {trending?.map((video) => (
                        <div key={video.Id} className="carousel-item" onClick={()=>onMovieClick(video)}>
                            <img src={video?.CoverImage} alt="" />
                        </div>
                    ))}
                </div>
            </div>
        </div>

    )
}