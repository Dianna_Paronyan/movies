
import Home from "./pages/Home";
import MainMenu from "./components/MainMenu";
import './App.css'
function App() {

  return (
    <div className="App">
        <MainMenu className="MainMenu" />
        <Home />
    </div>
  )
}

export default App
