import {useEffect, useState} from "react";
import {getData} from "../helpers/data";
import FeaturedVideo from "../components/FeaturedVideo";
import Trending from "../components/Trending";

export default function Home(){
    const [data, setData] = useState([])
    const [clickedMovie, setClickedMovie] = useState(null);

    useEffect(()=>{
        async function fetchData(){
            const res = await getData();
            setData(res);
        }
        fetchData()
    },[])

    useEffect(() => {
        const storedClickedMovie = sessionStorage.getItem('clickedMovie');
        if (storedClickedMovie) {
            const clickedMovieId = JSON.parse(storedClickedMovie);
            const trendingNow = data[0]?.TrendingNow || [];
            const sortedData = [...trendingNow].sort((a, b) =>
                a.Id === clickedMovieId ? -1 : b.Id === clickedMovieId ? 1 : 0
            );
            setData([{ ...data[0], TrendingNow: sortedData }]);
        }
    }, [clickedMovie]);

    function handleMovieClick(movie){
        sessionStorage.setItem('clickedMovie', JSON.stringify(movie.Id));
        setClickedMovie(movie);
    }
    return (
        <div style={{display:'flex', flexDirection:'column', position:'relative'}}>
            <FeaturedVideo videoData={data[0]?.Featured} clickedMovie={clickedMovie} />
            <Trending trending={data[0]?.TrendingNow} onMovieClick={handleMovieClick} />
        </div>
    )
}